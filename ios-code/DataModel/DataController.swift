//
//  DataController.swift
//  ios-code
//
//  Created by Steve Andrade on 1/12/23.
//

import Foundation
import CoreData

class DataController: ObservableObject {
    let container = NSPersistentContainer(name: "FavoritosModel")
    
    
    init(){
        container.loadPersistentStores{ desc, error in
            if let error = error  {
                print("Filed to load the data \(error.localizedDescription)")
            }
            
        }
    }
    
    func save(context: NSManagedObjectContext) {
        do {
            try context.save()
            print("Data save!!!")
        } catch {
            print("Not save data")
        }
    }
    
    func addFavoritos(name: String, url: String, context: NSManagedObjectContext) {
        let fav = Favoritos(context: context)
        
        fav.name = name
        fav.url = url
        
        save(context: context)
    }
    
}
