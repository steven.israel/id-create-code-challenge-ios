//
//  ios_codeApp.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import SwiftUI

@main
struct ios_codeApp: App {
    @StateObject private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                TabViewApp().environment(\.managedObjectContext, dataController.container.viewContext)
            }.navigationViewStyle(.stack)
            
        }
    }
}
