//
//  NetwortManager.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import Foundation
import Alamofire

class NetwortManager: NSObject {
    
    var services = NetwortService()
    
    var parameters = Parameters()
    var headers = HTTPHeaders()
    var method: HTTPMethod!
    var url :String! = baseURL
    var urlString = ""
    var type = 1
    var encoding: ParameterEncoding! = JSONEncoding.default
    
    public enum APIError: Error {
        case error(_ errorString: String)
        case info(_ errorBool: Bool)
    }
    
    init(data: [String:Any], headers: [String:String] = [:], url :String?, service :services? = nil,  method: HTTPMethod = .post, isJSONRequest: Bool = true, type: Int){
        super.init()
        
        data.forEach{parameters.updateValue($0.value, forKey: $0.key)}
        
        headers.forEach({self.headers.add(name: $0.key, value: $0.value)})
        self.headers.add(name: "Accept", value: "application/json")
        
        if type == 1 {
            if url == nil, service != nil{
                self.url += service!.rawValue
            }else{
                self.url = "\(baseURL)\(url ?? "")"
            }
        } else {
            self.url = url ?? ""
        }
        
        
        urlString = self.url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        self.type = type
        
        if !isJSONRequest{
            encoding = URLEncoding.default
        }
        self.method = method
        print("Service: \(urlString ) \n data: \(parameters)")
    }
    
    func executeQuery<T>(completion: @escaping (Result<ApiResponsePokemon<T>, APIError>) -> Void) where T: Codable {
        
        AF.request(urlString, method: method, parameters: parameters, encoding: encoding, headers: headers).responseData(completionHandler: {response in
            switch response.result{
                
                case .success(let res):
                    if let code = response.response?.statusCode{
                        switch code {
                            case 200...299:
                                do {
                                    let decodedData = try JSONDecoder().decode(ApiResponsePokemon<T>.self, from: res)
                                    
                                    completion(.success(decodedData))
                                } catch {
                                    print(String(data: res, encoding: .utf8) ?? "nothing received")
                                    completion(.failure(.error("Error: nothing received")))
                                    print("Error: nothing received \(error)")
                                    return
                                    //completion(.failure(error))
                                }
                            case 401:
                                print("Error 401")
                                completion(.failure(.info(true)))
                            default:
                                let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                                //completion(.failure(error))
                                print(error)
                                completion(.failure(.error("Error Code: \(error)")))
                        }
                    }
                case .failure(let error):
                    //completion(.failure(error))
                    print(error)
                    completion(.failure(.error("Error Catch: \(error.localizedDescription)")))
            }
        })
    }
    
    func executeQuery(completion: @escaping (Result<PokemonDetailModel, APIError>) -> Void) {
        
        AF.request(urlString, method: method, parameters: parameters, encoding: encoding, headers: headers).responseData(completionHandler: {response in
            switch response.result{
                
                case .success(let res):
                    if let code = response.response?.statusCode{
                        switch code {
                            case 200...299:
                                do {
                                    let decodedData = try JSONDecoder().decode(PokemonDetailModel.self, from: res)
                                    
                                    completion(.success(decodedData))
                                } catch {
                                    print(String(data: res, encoding: .utf8) ?? "nothing received")
                                    completion(.failure(.error("Error: nothing received")))
                                    print("Error: nothing received \(error)")
                                    return
                                    //completion(.failure(error))
                                }
                            case 401:
                                print("Error 401")
                                completion(.failure(.info(true)))
                            default:
                                let error = NSError(domain: response.debugDescription, code: code, userInfo: response.response?.allHeaderFields as? [String: Any])
                                //completion(.failure(error))
                                print(error)
                                completion(.failure(.error("Error Code: \(error)")))
                        }
                    }
                case .failure(let error):
                    //completion(.failure(error))
                    print(error)
                    completion(.failure(.error("Error Catch: \(error.localizedDescription)")))
            }
        })
    }
}

/**
 *check connectivity
*/
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
