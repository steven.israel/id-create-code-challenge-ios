//
//  AppError.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import Foundation

struct AppError: Identifiable {
    let id = UUID().uuidString
    let errorString: String
}
