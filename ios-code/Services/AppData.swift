//
//  AppData.swift
//  ios-code
//
//  Created by Steve Andrade on 1/12/23.
//

import Foundation

class AppData {
    static let sharedData = AppData()
    
    func getFavoritos() -> [PokemonModel]? {
        if let data = UserDefaults.standard.object(forKey: "favoritos") as? Data {
            if let favoritos = try? JSONDecoder().decode([PokemonModel].self, from: data) {
                return favoritos
            }
        }
        return []
    }

    // save user logged
    func saveFavorito(with data: [PokemonModel]) {
        if let encoded = try? JSONEncoder().encode(data) {
            UserDefaults.standard.set(encoded, forKey: "favoritos")
            UserDefaults.standard.synchronize()
        }
    }
}
