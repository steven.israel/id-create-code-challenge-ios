//
//  PokemonListView.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import SwiftUI

struct PokemonListView: View {
    @EnvironmentObject var viewModel: PokemonViewModel
    
    var body: some View {
        ZStack {
            Color("color2").ignoresSafeArea()
            VStack {
                ScrollView {
                    LazyVGrid (columns: viewModel.formaGrid, spacing: 8) {
                        ForEach(viewModel.listPokemon, id: \.self) {
                            pokemon in
                            NavigationLink(destination: pokemonDetailView(pokemon: pokemon).environmentObject(viewModel)
                            ) {
                                PokemonCard(id: viewModel.getIdSplit(pokemon: pokemon),name: pokemon.name)
                            }.padding(.bottom, 33)
                        }
                    }.padding(.horizontal, 33)
                }
            }
        }
        .onAppear(perform: {
            viewModel.getPokemons(limit: 20)
        })
    }
}

struct PokemonListView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonListView().environmentObject(PokemonViewModel())
    }
}
