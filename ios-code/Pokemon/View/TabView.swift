//
//  Tabview.swift
//  ios-code
//
//  Created by Steve Andrade on 1/10/23.
//

import SwiftUI

struct Tabview: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct Tabview_Previews: PreviewProvider {
    static var previews: some View {
        Tabview()
    }
}
