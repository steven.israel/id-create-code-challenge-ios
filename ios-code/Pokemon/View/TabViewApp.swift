//
//  TabViewApp.swift
//  ios-code
//
//  Created by Steve Andrade on 1/10/23.
//

import SwiftUI

struct TabViewApp: View {
    @ObservedObject var viewModel = PokemonViewModel()
    
    var body: some View {
        ZStack {
            Color("color2").ignoresSafeArea()
            VStack {
                
                if viewModel.connection {
                    ErrorConnectionView().environmentObject(viewModel)
                } else {
                    NavigationView {
                        TabView (selection: $viewModel.tabSeleccionado) {
                            PokemonListView().tabItem {
                                Image(systemName: "house.fill")
                            }.tag(0)
                            
                           FavoritosView().tabItem {
                                Image(systemName: "star")
                            }.tag(3)
                        }.accentColor(Color("color3")).environmentObject(viewModel)
                    }.navigationViewStyle(StackNavigationViewStyle())
                }
                
            }.navigationTitle("")
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarBackButtonHidden(true)
        }
    }
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor(Color("color1"))
        UITabBar.appearance().unselectedItemTintColor = UIColor(Color("color2"))
        UITabBar.appearance().isTranslucent = true
    }
}

struct TabViewApp_Previews: PreviewProvider {
    static var previews: some View {
        TabViewApp()
    }
}
