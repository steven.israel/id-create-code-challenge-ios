//
//  pokemonDetailView.swift
//  ios-code
//
//  Created by Steve Andrade on 1/11/23.
//

import SwiftUI
import NukeUI

struct pokemonDetailView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) var managedObjContext
    //@Environment(\.dismiss) var dismiss
    @FetchRequest(sortDescriptors: [SortDescriptor(\.name, order: .reverse)]) var fav: FetchedResults<Favoritos>
    
    @EnvironmentObject var viewModel: PokemonViewModel
    let pokemon: PokemonModel
    @State var scale = 1.0
        
    var body: some View {
        ZStack {
            Color("color2").ignoresSafeArea()
            GeometryReader { geometry in
                ScrollView {
                
                    VStack {
                        
                        VStack {
                            ZStack {
                                
                                LazyImage(source: viewModel.detailPokemon?.sprites.front_default) { state in
                                    if let image = state.image {
                                        image
                                    } else if state.error != nil {
                                        ProgressView().tint(Color("color3"))
                                    } else {
                                        ProgressView().tint(Color("color3"))
                                    }
                                }
                                .frame(width: 200, height: 200, alignment: .center)
                                
                                VStack {
                                    if viewModel.tabSeleccionado == 0 {
                                        
                                        Button(action: {
                                            scale += 0.5
                                            
                                            DataController().addFavoritos(name: pokemon.name, url: pokemon.url, context: managedObjContext)
                                            
                                        }, label: {
                                            Image(systemName: "heart.fill").resizable()
                                                .foregroundColor(viewModel.tabSeleccionado == 0 ? Color("color3") : Color("color2"))
                                                .frame(width: 30, height: 30, alignment: .leading)
                                                .padding(.trailing, 20)
                                        })
                                        .scaleEffect(scale)
                                        .animation(.linear(duration: 1), value: scale)
                                        
                                        
                                    }
                                    
                                    Button(action: {
                                        scale += 0.5
                                        
                                        compartir(name: pokemon.name)
                                        
                                    }, label: {
                                        Image(systemName: "square.and.arrow.up").resizable()
                                            .foregroundColor(viewModel.tabSeleccionado == 0 ? Color("color3") : Color("color2"))
                                            .frame(width: 25, height: 25, alignment: .leading)
                                            .padding(.trailing, 20)
                                    })
                                    .scaleEffect(scale)
                                    .animation(.linear(duration: 1), value: scale)
                                    
                                }.frame(maxWidth: .infinity, maxHeight: 200, alignment: .leading)
                                    .padding(.leading, 23)
                                    .offset(y: 50)
                                
                            }.frame(maxWidth: .infinity, alignment: .center)
                            
                            
                        }.frame(maxWidth: geometry.size.width)
                            .background(Color("color1"))
                    
                        
                        
                        VStack (alignment: .leading) {
                            Group {
                                ScrollView (.horizontal, showsIndicators: false){
                                    HStack {
                                        InfoCardView(texto: "Altura: \(viewModel.detailPokemon?.height ?? 0)")
                                        InfoCardView(texto: "Experiencia base: \(viewModel.detailPokemon?.base_experience ?? 0)")
                                        InfoCardView(texto: "Generación \(viewModel.detailPokemon?.order ?? 0)")
                                    }
                                }
                                
                                Text("ID número: \(viewModel.detailPokemon?.id ?? 0)")
                                    .foregroundColor(Color("color1"))
                                    .padding(1)
                                
                                Text("Nombre: \(viewModel.detailPokemon?.name ?? "")")
                                    .foregroundColor(Color("color1"))
                                    .padding(1)
                                
                                Text("Weight: \(viewModel.detailPokemon?.weight ?? 0)")
                                    .foregroundColor(Color("color1"))
                                    .padding(1)
                                
                                Text("Estadistícas:")
                                    .foregroundColor(Color("color1"))
                                
                                ScrollView (.horizontal, showsIndicators: false){
                                    
                                    HStack {
                                        ForEach(viewModel.detailPokemon?.stats ?? [], id: \.self) { stat in
                                            InfoCardView(texto: "\(stat.stat.name)")
                                        }
                                    }
                                    
                                }
                            }
                            
                            Group {
                                
                                Text("Habilidades:")
                                        .foregroundColor(Color("color3"))
                                        .fontWeight(.bold)
                                        .padding(.top, 30)
                                
                                ScrollView (.horizontal, showsIndicators: false){
                                    
                                    HStack {
                                        ForEach(viewModel.detailPokemon?.abilities ?? [], id: \.self) { ability in
                                            InfoCardView(texto: "\(ability.ability.name)")
                                        }
                                    }
                                    
                                }
                                
                                Text("Articulos cargados:")
                                        .foregroundColor(Color("color3"))
                                        .fontWeight(.bold)
                                        .padding(.top, 30)
                                
                                ScrollView (.horizontal, showsIndicators: false){
                                    
                                    HStack {
                                        ForEach(viewModel.detailPokemon?.held_items ?? [], id: \.self) { held in
                                            InfoCardView(texto: "\(held.item.name)")
                                        }
                                    }
                                    
                                }
                                
                                Spacer()
                                
                                Button(action: {
                                    self.presentationMode.wrappedValue.dismiss()
                                }, label: {
                                    VStack {
                                        Text("Regresar")
                                            .fontWeight(.bold)
                                            .foregroundColor(Color("color1"))
                                            .frame(width: geometry.size.width / 3, height: 40, alignment: .center)
                                            .background(
                                            LinearGradient(gradient: Gradient(colors: [Color("color3"), Color("color3")]), startPoint: .leading, endPoint: .trailing)
                                        )
                                        .cornerRadius(10)
                                    }.frame(maxWidth: .infinity, alignment: .center)
                                })
                            }
                            
                            
                            
                        }.frame(maxWidth: geometry.size.width)
                            .padding(.leading, 23)
                            .padding(.trailing, 5)
                        
                        
                    }
                }
            }
        }.navigationBarBackButtonHidden(true)
        .onAppear{
            viewModel.getPokemonDetail(url: pokemon.url)
        }
        
    }
    
    private func compartir(name: String) {
        let urlWa = "whatsapp://send?text=Te comparto uno de mis pokemones favoritos: \(name)"
        
        if let urlString = urlWa.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
            if let whaUrl = NSURL(string: urlString){
                if UIApplication.shared.canOpenURL(whaUrl as URL){
                    UIApplication.shared.open(whaUrl as URL)
                }
            }
            
        }
    }
}

struct pokemonDetailView_Previews: PreviewProvider {
    static var previews: some View {
        pokemonDetailView(pokemon: PokemonModel.getMockData()).environmentObject(PokemonViewModel())
    }
}
