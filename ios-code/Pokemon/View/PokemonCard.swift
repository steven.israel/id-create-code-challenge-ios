//
//  PokemonCard.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import SwiftUI
import NukeUI

struct PokemonCard: View {
    var id: String
    var name: String
    
    var body: some View {
        VStack {
            LazyImage(source: self.format_url(id: self.id)) { state in
                if let image = state.image {
                    image
                } else if state.error != nil {
                    ProgressView().tint(Color("color3"))
                } else {
                    ProgressView().tint(Color("color3"))
                }
            }
            .frame(width: 100, height: 100, alignment: .center)
            
            VStack(alignment: .leading) {
                
                Text("#\(self.id)").padding(.top, 11.48)
                    .foregroundColor(Color.black).padding(.leading, 10)
                
                Text("\(name)").foregroundColor(Color.black)
                    .padding(.leading, 10)
                    .padding(.bottom, 11.48)
                
            }.frame(maxWidth: .infinity, alignment: .leading)
                .background(Color("color1"))
                .cornerRadius(10)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color("color3"), lineWidth: 2)
                )
        }.overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color("color3"), lineWidth: 2)
        )
    }
    
    func format_url(id: String) -> URL? {
        let img = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png")
        
        return img ?? nil
    }
}

struct PokemonCard_Previews: PreviewProvider {
    static var previews: some View {
        PokemonCard(id: "1", name: "Pikachu")
    }
}
