//
//  InfoCardView.swift
//  ios-code
//
//  Created by Steve Andrade on 1/11/23.
//

import SwiftUI

struct InfoCardView: View {
    let texto: String
    
    var body: some View {
        VStack {
            Text("\(texto)").padding().foregroundColor(Color("color1"))
        }.overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color("color3"), lineWidth: 2)
        )
    }
}

struct InfoCardView_Previews: PreviewProvider {
    static var previews: some View {
        InfoCardView(texto: "Altura: 12")
    }
}
