//
//  FavoritosView.swift
//  ios-code
//
//  Created by Steve Andrade on 1/12/23.
//

import SwiftUI

struct FavoritosView: View {
    @EnvironmentObject var viewModel: PokemonViewModel
    @Environment(\.managedObjectContext) var managedObjContext
    @FetchRequest(sortDescriptors: [SortDescriptor(\.name, order: .reverse)]) var fav: FetchedResults<Favoritos>
    
    var body: some View {
        ZStack {
            Color("color2").ignoresSafeArea()
            VStack {
                ScrollView {
                    LazyVGrid (columns: viewModel.formaGrid, spacing: 8) {
                        ForEach(fav, id: \.self) { fav in
                            NavigationLink(destination: pokemonDetailView(pokemon: PokemonModel.getMockData(name: fav.name ?? "", url: fav.url ?? "")).environmentObject(viewModel)
                            ) {
                                PokemonCard(id: viewModel.getIdSplit(pokemon: PokemonModel.getMockData(name: fav.name ?? "", url: fav.url ?? "")),name: fav.name ?? "")
                            }.padding(.bottom, 33)
                        }
                    }.padding(.horizontal, 33)
                }
            }
        }
    }
}

struct FavoritosView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritosView().environmentObject(PokemonViewModel())
    }
}
