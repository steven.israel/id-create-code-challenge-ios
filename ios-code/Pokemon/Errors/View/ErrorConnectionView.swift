//
//  ErrorConnection.swift
//  ios-code
//
//  Created by Steve Andrade on 1/10/23.
//

import SwiftUI

struct ErrorConnectionView: View {
    @EnvironmentObject var viewModel: PokemonViewModel
    
    var body: some View {
        ZStack {
            Color("color3").ignoresSafeArea()
            VStack {
                Image("connectionError").resizable().scaledToFit().frame(width: 100)
                Text("Error de conexión").fontWeight(.bold)
                
                Button(action: {
                    viewModel.connectionStatus()
                }, label: {
                    Text("Recargar").foregroundColor(Color("color1")).padding(.top, 20)
                })
            }.navigationTitle("")
                .navigationBarTitleDisplayMode(.inline)
                .navigationBarBackButtonHidden(true)
        }
    }
}

struct ErrorConnectionView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorConnectionView().environmentObject(PokemonViewModel())
    }
}
