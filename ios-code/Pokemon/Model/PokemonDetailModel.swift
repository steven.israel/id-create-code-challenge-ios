//
//  PokemonDetailModel.swift
//  ios-code
//
//  Created by Steve Andrade on 1/11/23.
//

import Foundation

struct PokemonDetailModel: Codable {
    let id: Int
    let height: Int
    let weight: Int
    let order: Int
    let name: String
    let abilities: [AbilitiesModel]
    let base_experience: Int
    let sprites: SpritesModel
    let stats: [StatsModel]
    let held_items: [HeldItemsModel]
}

struct SpritesModel: Codable {
    let front_default: String
}

struct StatsModel: Codable, Hashable {
    let base_stat: Int
    let effort: Int
    let stat: StatModel
}

struct StatModel: Codable, Hashable {
    let name: String
    let url: String
}

struct AbilitiesModel: Codable, Hashable {
    let ability: AbilityModel
    let is_hidden: Bool
    let slot: Int
}

struct AbilityModel: Codable, Hashable {
    let name: String
    let url: String
}

struct HeldItemsModel: Codable,Hashable {
    let item: ItemModel
}

struct ItemModel: Codable, Hashable {
    let name: String
    let url: String
}
