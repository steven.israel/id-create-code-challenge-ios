//
//  PokemonModel.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import Foundation

struct PokemonModel: Codable, Hashable, Identifiable {
    let id = UUID()
    let name: String
    let url: String
    enum CodingKeys: String, CodingKey {
        case name, url
    }
}

extension PokemonModel {
    static func getMockData(
        name: String = "spearow",
        url: String = "https://pokeapi.co/api/v2/pokemon/21/"
    ) -> PokemonModel {
        .init(
            name: name,
            url: url
        )
    }
}
