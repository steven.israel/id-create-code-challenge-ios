//
//  ApiResponse.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import Foundation

struct ApiResponsePokemon<D: Decodable> {
    let count: Int
    let next: String
    let previous: String?
    let results: [D]
}

extension ApiResponsePokemon: Decodable {}
