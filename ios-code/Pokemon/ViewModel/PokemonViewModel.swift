//
//  PokemonViewModel.swift
//  ios-code
//
//  Created by Steve Andrade on 1/9/23.
//

import Foundation
import Combine
import SwiftUI

class PokemonViewModel: ObservableObject {
    
    @Published var listPokemon: [PokemonModel] = []
    @Published var tabSeleccionado: Int = 0
    @Published var detailPokemon: PokemonDetailModel?
    @Published var listFavoritos: [PokemonModel] = AppData.sharedData.getFavoritos() ?? []
    var appError: AppError? = nil
    @Published var connection: Bool = false
    @Published var isLoading: Bool = false
    @Published var formaGrid = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    func connectionStatus(){
        connection = false

        if !Connectivity.isConnectedToInternet() {
            print("Internet is not available.")
            connection = true
            return
        }
    }
    
    func getPokemons(limit: Int) {
        
        isLoading = true
        connectionStatus()
        
        // MARK: Network call
        // get call - model departamentos
        NetwortManager(data: [:], headers: [:], url: "\(services.wsGetPokemons.rawValue)?limit=\(limit)", service: nil, method: .get, isJSONRequest: false, type: 1).executeQuery() {
            
            (result: Result<ApiResponsePokemon<PokemonModel>,NetwortManager.APIError>) in
            switch result{
            case .success(let response):
                print(response)
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.listPokemon = response.results
                }
                //self.resultData = response.data ?? .init()
            case .failure(let error):
                switch error {
                    case .error(let errorString):
                        self.isLoading = false
                        self.appError = AppError(errorString: errorString)
                        print(errorString)
                    case .info(let errorBool):
                        self.appError = AppError(errorString: "Error: Intente nuevamente")
                        print(errorBool)
                }
            }
        }
    }
    
    func getPokemonDetail(url: String) {
        
        isLoading = true
        connectionStatus()
        
        // MARK: Network call
        // get call - model departamentos
        NetwortManager(data: [:], headers: [:], url: url, service: nil, method: .get, isJSONRequest: false, type: 2).executeQuery() {
            
            (result: Result<PokemonDetailModel,NetwortManager.APIError>) in
            switch result{
            case .success(let response):
                print(response)
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.detailPokemon = response
                }
                //self.resultData = response.data ?? .init()
            case .failure(let error):
                switch error {
                    case .error(let errorString):
                        self.isLoading = false
                        self.appError = AppError(errorString: errorString)
                        print(errorString)
                    case .info(let errorBool):
                        self.appError = AppError(errorString: "Error: Intente nuevamente")
                        print(errorBool)
                }
            }
        }
    }
    
    func getIdSplit(pokemon: PokemonModel) -> String {
        let pos = pokemon.url.components(separatedBy: "/")
        
        return pos[6]
    }
}
